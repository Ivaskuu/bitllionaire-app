package com.skuulabs.bitllionaire;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Adrian on 09/10/2017.
 */

public class StartMonitoringServiceOnBoot extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        if(intent.getAction() == Intent.ACTION_BOOT_COMPLETED)
        {
            Intent monitoringServiceIntent = new Intent(context, MonitorCoinsService.class);
            context.startService(monitoringServiceIntent);
        }
    }
}
