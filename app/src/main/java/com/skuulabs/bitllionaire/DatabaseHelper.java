package com.skuulabs.bitllionaire;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Adrian on 07/10/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper
{
    private final static String TABLE_NAME = "monitored_coins";

    public static final String COL1 = "coin_short_name";
    public static final String COL2 = "coin_long_name";
    public static final String COL3 = "coin_symbol";
    public static final String COL4 = "alert_type";
    public static final String COL5 = "value";
    public static final String COL6 = "percent";
    public static final String COL7 = "active";
    public static final String COL8 = "notificationID";
    public static final String COL9 = "isShowingNotification";
    public static final String COL10 = "hold";

    DatabaseHelper(Context context)
    {
        super(context, TABLE_NAME, null, 6);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String createTable = "CREATE TABLE " + TABLE_NAME + " (" +
                COL1 + " VARCHAR NOT NULL PRIMARY KEY, " +
                COL2 + " VARCHAR NOT NULL, " +
                COL3 + " TINYINT UNSIGNED NOT NULL, " +
                COL4 + " FLOAT UNSIGNED NOT NULL, " +
                COL5 + " FLOAT UNSIGNED NOT NULL, " +
                COL6 + " BIT NOT NULL, " +
                COL7 + " INTEGER UNSIGNED NOT NULL, " +
                COL8 + " TEXT, " +
                COL9 + " BIT NOT NULL, " +
                COL10 + " FLOAT UNSIGNED NOT NULL)";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean addData(CoinInfos coinInfos)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COL1, coinInfos.shortName);
        values.put(COL2, coinInfos.longName);
        values.put(COL3, coinInfos.symbol);
        values.put(COL4, coinInfos.alertType);
        values.put(COL5, coinInfos.value);
        values.put(COL6, coinInfos.percent);
        values.put(COL7, coinInfos.active);
        values.put(COL8, coinInfos.notificationID);
        values.put(COL9, 0);
        values.put(COL10, coinInfos.hold);

        if(db.insert(TABLE_NAME, null, values) == -1)
        {
            return false;
        }

        return true;
    }

    public Cursor getData(String orderString)
    {
        String query = "SELECT * FROM " + TABLE_NAME + " " + orderString;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(query, null);

        return data;
    }

    public CoinInfos getCoinData(String coinName)
    {
        String query = "SELECT * FROM " + TABLE_NAME + " "
                + "WHERE " + COL1 + " = \"" + coinName + "\"";

        Cursor data = this.getWritableDatabase().rawQuery(query, null);
        if (data != null)
        {
            data.moveToFirst();
            return new CoinInfos
                    (
                            data.getString(0),
                            data.getString(1),
                            data.getString(2),
                            data.getInt(3),
                            data.getFloat(4),
                            data.getFloat(5),
                            data.getInt(6),
                            0f,
                            data.getInt(7),
                            data.getFloat(8)
                    );
        }

        return null;
    }

    public void deleteAll()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME);
    }

    public void deleteFromTable(String coinName)
    {
        String query = "DELETE FROM " + TABLE_NAME + " "
                + "WHERE " + COL1 + " = \"" + coinName + "\"";

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    public void updateCoinActiveValue(String coinName, int activeValue)
    {
        String query = "UPDATE " + TABLE_NAME + " "
                + "SET " + COL7 + " = " + activeValue + " "
                + "WHERE " + COL1 + " = \"" + coinName + "\"";

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    public void updateCoinPercent(String coinName, float newPerc)
    {
        String query = "UPDATE " + TABLE_NAME + " "
                + "SET " + COL6 + " = " + newPerc + " "
                + "WHERE " + COL1 + " = \"" + coinName + "\"";

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    public void updateHold(String coinName, float newHold)
    {
        String query = "UPDATE " + TABLE_NAME + " "
                + "SET " + COL10 + " = " + newHold + " "
                + "WHERE " + COL1 + " = \"" + coinName + "\"";

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    public void updateAlertType(String coinName, int alertType)
    {
        String query = "UPDATE " + TABLE_NAME + " "
                + "SET " + COL4 + " = " + alertType + " "
                + "WHERE " + COL1 + " = \"" + coinName + "\"";

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    public void toggleIsShowingNotification(boolean value, String coinName)
    {
        int state = value ? 1 : 0;

        String query = "UPDATE " + TABLE_NAME + " "
                + "SET " + COL9 + " = " + state + " "
                + "WHERE " + COL1 + " = \"" + coinName + "\"";

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    public boolean getIsShowingNotification(String coinName)
    {
        String query = "SELECT * FROM " + TABLE_NAME + " "
                + "WHERE " + COL1 + " = \"" + coinName + "\"";

        Cursor data = this.getWritableDatabase().rawQuery(query, null);
        if (data != null)
        {
            data.moveToFirst();
            return data.getInt(8) == 1 ? true : false;
        }

        return false;
    }
}