package com.skuulabs.bitllionaire;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.db.chart.view.LineChartView;

import org.json.JSONArray;
import org.json.JSONException;

public class CoinActivity extends AppCompatActivity
{
    private CoinInfos coin = new CoinInfos();

    private TextView coinValueTextView;
    private TextView percTextView;
    private FloatingActionButton fab;

    private int resultCode = RESULT_CANCELED;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coin);

        /* Set the action bar */
        setSupportActionBar((Toolbar)this.findViewById(R.id.coinToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        /* Set the status bar color */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }

        /* Get the coin infos from the main activity */
        coin.shortName = getIntent().getExtras().getString("shortName");
        coin.longName = getIntent().getExtras().getString("longName");
        coin.symbol = getIntent().getExtras().getString("symbol");
        coin.alertType = getIntent().getExtras().getInt("alertType");
        coin.value = getIntent().getExtras().getFloat("value");
        coin.percent = getIntent().getExtras().getFloat("percent");
        coin.active = getIntent().getExtras().getInt("active");
        coin.notificationID = getIntent().getExtras().getInt("notificationID");
        coin.newValue = 0;

        /* Find the views */
        TextView coinNameText = (TextView)this.findViewById(R.id.coinName);
        coinValueTextView = (TextView)this.findViewById(R.id.coinValue);
        percTextView = (TextView)this.findViewById(R.id.coinPerc);
        fab = (FloatingActionButton) this.findViewById(R.id.coinMonitoringFab);
        LineChartView chart = (LineChartView)this.findViewById(R.id.linechart);

        /* Set the view content with the coin values from main */
        coinNameText.setText(coin.longName);
        coinValueTextView.setText("$ " + String.format("%.6f", coin.value));
        percTextView.setText("Just a moment ...");
        fab.setImageResource(coin.active == 1 ? R.drawable.ic_pause_white_24dp : R.drawable.ic_play_arrow_white_24dp);

        /* Set the toolbar and status bar color based on the coin active value */
        this.findViewById(R.id.coinToolbar).setBackgroundColor(coin.active == 1 ? ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinEnabled) : ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinDisabled));
        this.findViewById(R.id.toolbarLinearLayout).setBackgroundColor(coin.active == 1 ? ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinEnabled) : ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinDisabled));
        getWindow().setStatusBarColor(coin.active == 1 ? ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinEnabled) : ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinDisabled));

        downloadCoinValue();

        /* Init the chart
        LineSet lineSet = new LineSet();
        lineSet.setColor(Color.WHITE);
        lineSet.setSmooth(true);
        chart.setXAxis(false);
        chart.setXLabels(AxisRenderer.LabelPosition.NONE);
        chart.setYAxis(false);
        chart.setYLabels(AxisRenderer.LabelPosition.NONE);

        DatabaseHelper db = new DatabaseHelper(CoinActivity.this);
        String json = db.getCoinValuesJson(coin.shortName);

        int zoom;
        if(coin.value > 500) zoom = 7;
        else if (coin.value > 50) zoom = 9;
        else if (coin.value > 25) zoom = 11;
        else if (coin.value > 15) zoom = 13;
        else if (coin.value > 10) zoom = 15;
        else if (coin.value > 5) zoom = 17;
        else if (coin.value > 1) zoom = 19;
        else zoom = 21;

        try
        {
            JSONArray jsonArray = new JSONArray(json);
            for(int i = 0; i < jsonArray.length(); i++)
            {
                lineSet.addPoint(new Point("", (float)Math.pow(jsonArray.getDouble(i), zoom)));
            }

            chart.addData(lineSet);
            chart.show();
        }
        catch (Exception ex)
        {
            Log.e("cchart", "err");
        }*/
    }

    private void downloadCoinValue()
    {
        String url = "https://api.coinmarketcap.com/v1/ticker/" + coin.shortName + "/";
        StringRequest btcPriceRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        // Checks the new coin value
                        try
                        {
                            JSONArray json = new JSONArray(response);
                            coin.newValue = (float)json.getJSONObject(0).getDouble("price_usd");

                            coinValueTextView.setText("$ " + String.format("%.6f", coin.newValue));

                            float perc = ((coin.newValue - coin.value) / coin.value) * 100;
                            percTextView.setText(perc >= 0 ? "(+" + String.format("%.2f", perc) + " %)" : "(" + String.format("%.2f", perc) + " %)");
                        }
                        catch (JSONException e)
                        {
                            Log.e("Http", "Non riesco a decodificare il JSON", e);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Http", "Erore accessing CMC coin value");
                    }
                }
        );

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(btcPriceRequest);
    }

    public void toggleCoinMonitoring(View view)
    {
        if(coin.active == 0)
        {
            fab.setImageResource(R.drawable.ic_pause_white_24dp);
            coin.active = 1;
            this.findViewById(R.id.coinToolbar).setBackgroundColor(ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinEnabled));
            this.findViewById(R.id.toolbarLinearLayout).setBackgroundColor(ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinEnabled));
            getWindow().setStatusBarColor(coin.active == 1 ? ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinEnabled) : ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinDisabled));

            Toast.makeText(CoinActivity.this, "Monitoring enabled", Toast.LENGTH_SHORT).show();
        }
        else
        {
            fab.setImageResource(R.drawable.ic_play_arrow_white_24dp);
            coin.active = 0;
            this.findViewById(R.id.coinToolbar).setBackgroundColor(ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinDisabled));
            this.findViewById(R.id.toolbarLinearLayout).setBackgroundColor(ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinDisabled));
            getWindow().setStatusBarColor(coin.active == 1 ? ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinEnabled) : ContextCompat.getColor(CoinActivity.this, R.color.coinActivityCoinDisabled));

            Toast.makeText(CoinActivity.this, "Monitoring disabled", Toast.LENGTH_SHORT).show();
        }

        DatabaseHelper db = new DatabaseHelper(CoinActivity.this);
        db.updateCoinActiveValue(coin.shortName, coin.active);

        setResult(RESULT_OK);
    }

    public void resetCoinvalue(View view)
    {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Are you sure you want to reset the coin value ?");
        alert.setMessage("This means that it's percentage will return to 0%");

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int whichButton)
            {
                DatabaseHelper db = new DatabaseHelper(CoinActivity.this);
                db.deleteFromTable(coin.shortName);

                coin.value = coin.newValue;
                db.addData(coin);

                setResult(RESULT_OK);
                // TODO better
            }
        });

        alert.setNegativeButton("Cancel", null);
        alert.show();
    }

    public void changeMonitoringType(View view)
    {
        DatabaseHelper db = new DatabaseHelper(CoinActivity.this);
        Toast.makeText(this, "Not implemented yet...", Toast.LENGTH_SHORT).show();
    }

    public void changeAlertPercent(View view)
    {
        final DatabaseHelper db = new DatabaseHelper(CoinActivity.this);

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(CoinActivity.this);

        edittext.setText(coin.percent + "");
        edittext.setHint("The new percent");

        alert.setTitle("Enter the new alert percent");
        alert.setView(edittext);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int whichButton)
            {
                db.updateCoinPercent(coin.shortName, Float.parseFloat(edittext.getText().toString()));
                coin.percent = Float.parseFloat(edittext.getText().toString());

                setResult(RESULT_OK);
            }
        });

        alert.setNegativeButton("Cancel", null);
        alert.show();
    }

    public void removeCoin(View view)
    {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Are you sure you want to delete this coin ?");
        alert.setMessage("You won't receive no more alerts of this coin, and the coin chart will be lost.");

        alert.setPositiveButton("DELETE", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int whichButton)
            {
                DatabaseHelper db = new DatabaseHelper(CoinActivity.this);
                db.deleteFromTable(coin.shortName);

                setResult(RESULT_OK);
                finish();
            }
        });

        alert.setNegativeButton("CANCEL", null);
        alert.show();
    }

    String coinToString(double val)
    {
        if(val < 0.0000001)
        {
            String.format("%.8f", coin.newValue);
        }

        return null;
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        onBackPressed();
        return true;
    }
}
