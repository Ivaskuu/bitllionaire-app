package com.skuulabs.bitllionaire;

import android.content.Intent;
import android.view.View;

import com.luseen.verticalintrolibrary.VerticalIntro;
import com.luseen.verticalintrolibrary.VerticalIntroItem;

public class OnboardingActivity extends VerticalIntro
{
    @Override
    protected void init()
    {
        addIntroItem(new VerticalIntroItem.Builder()
                .backgroundColor(R.color.white)
                .image(R.drawable.hello)
                .title("Welcome to Bitllionaire !")
                .titleColor(R.color.black)
                .text("Bitllionaire let's you receive real time notifications and alerts for your favorite cryptocoins !\n\n\n\n")
                .textColor(android.R.color.secondary_text_dark)
                .textSize(16)
                .titleSize(28)
                .nextTextColor(R.color.black)
                .build());

        addIntroItem(new VerticalIntroItem.Builder()
                .backgroundColor(R.color.white)
                .image(R.drawable.bitcoin_money_circle_icon)
                .title("Bitllionaire is simple !")
                .titleColor(R.color.black)
                .text("Just add a coin, set a percentage for the alert and...\n\n\n\n")
                .textColor(android.R.color.secondary_text_dark)
                .textSize(16)
                .titleSize(28)
                .nextTextColor(R.color.black)
                .build());

        addIntroItem(new VerticalIntroItem.Builder()
                .backgroundColor(R.color.white)
                .image(R.drawable.beach) // Beach image
                .title("Relax !")
                .titleColor(R.color.black)
                .text("You can forget about it, we're handling it!\n\n\n\n")
                .textColor(android.R.color.secondary_text_dark)
                .textSize(16)
                .titleSize(28)
                .nextTextColor(R.color.white)
                .build());

        setSkipEnabled(true);
        setSkipColor(android.R.color.secondary_text_dark);
        setVibrateEnabled(false);
        setNextText("NEXT");
        setDoneText("I AM READY");
    }

    @Override
    protected Integer setLastItemBottomViewColor()
    {
        return R.color.colorPrimaryDark;
    }

    @Override
    protected void onSkipPressed(View view) {}

    @Override
    protected void onFragmentChanged(int position) {}

    @Override
    protected void onDonePressed()
    {
        getSharedPreferences(getResources().getString(R.string.globalPrefsName), MODE_PRIVATE).edit().putBoolean(getResources().getString(R.string.prefsOnboard), true).commit();

        Intent intent = new Intent(OnboardingActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}