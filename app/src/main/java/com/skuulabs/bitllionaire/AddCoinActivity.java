package com.skuulabs.bitllionaire;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import io.ghyeok.stickyswitch.widget.StickySwitch;

import static com.skuulabs.bitllionaire.R.id.alertTypeSwitch;

public class AddCoinActivity extends AppCompatActivity
{
    private ProgressDialog progressDialog;
    private ProgressDialog progressDialog2;

    private DiscreteSeekBar percBar;
    private TextView alertDescription;
    private TextView textCoinNotExist;

    private int alertType = 0;

    private ArrayList<String> coins = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_coin);

        percBar = (DiscreteSeekBar) this.findViewById(R.id.percBar);
        alertDescription = (TextView)this.findViewById(R.id.alertDescriptionText);
        textCoinNotExist = (TextView)this.findViewById(R.id.coinNotExistText);

        alertDescription.setText(getString(R.string.block_description, percBar.getProgress()));

        /* Init the progress dialog */
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Searching for the coin");
        progressDialog.setMessage("Give us a second...");
        progressDialog.setCancelable(false);

        /* Init the coin downloading progress dialog */
        progressDialog2 = new ProgressDialog(this);
        progressDialog2.setTitle("Downloading the coins list");
        progressDialog2.setMessage("Give us a second please ...");
        progressDialog2.setCancelable(false);

        /* Change the descriptionText on alertTypeSwitch press */
        final StickySwitch stickySwitch = (StickySwitch) findViewById(alertTypeSwitch);
        stickySwitch.setOnSelectedChangeListener(new StickySwitch.OnSelectedChangeListener()
        {
            @Override
            public void onSelectedChange(@NotNull StickySwitch.Direction direction, @NotNull String text)
            {
                if(direction.name() == "RIGHT")
                {
                    alertType = 1;
                    alertDescription.setText(getString(R.string.up_description, percBar.getProgress()));

                    //stickySwitch.setDirection(StickySwitch.Direction.LEFT);
                }
                else
                {
                    alertType = 0;
                    alertDescription.setText(getString(R.string.block_description, percBar.getProgress()));
                }
            }
        });

        /* Download the coins list */
        downloadCoinsList();
    }

    public void downloadCoinsList()
    {
        progressDialog2.show();
        String url = "https://api.coinmarketcap.com/v1/ticker/";

        StringRequest coinExistsRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        try
                        {
                            /* Get the coin list */
                            JSONArray json = new JSONArray(response);
                            int coinsNum = json.length();

                            for(int i = 0; i < coinsNum; i++)
                            {
                                coins.add(json.getJSONObject(i).getString("id"));
                            }

                            /* Load coins from the database */
                            ArrayList<String> alreadyAddedCoins = new ArrayList<>();
                            DatabaseHelper databaseHelper = new DatabaseHelper(AddCoinActivity.this);
                            final Cursor coinsData = databaseHelper.getData("");

                            // TODO: Optimize

                            while (coinsData.moveToNext())
                            {
                                alreadyAddedCoins.add(coinsData.getString(0));
                            }

                            for(int i = 0; i < coins.size(); i++) /* Using .size() because coins may get removed */
                            {
                                for(int j = 0; j < alreadyAddedCoins.size(); j++)
                                {
                                    if(coins.get(i).equals(alreadyAddedCoins.get(j)))
                                    {
                                        coins.remove(i);
                                        i--;
                                        break;
                                    }
                                }
                            }

                            /* Set the autocomplete strings */
                            AutoCompleteTextView coinTextView = (AutoCompleteTextView)findViewById(R.id.cryptoName);
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(AddCoinActivity.this, android.R.layout.simple_list_item_1, coins.toArray(new String[coins.size()]));
                            coinTextView.setAdapter(adapter);

                            progressDialog2.dismiss();
                        }
                        catch (JSONException e)
                        {
                            Log.e("Http", "JSON decoding error in addcoin activity: " + e.toString());
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Http", "Erore accessing CMC coin value: " + error.toString());
                        textCoinNotExist.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                    }
                }
        );

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(coinExistsRequest);
    }

    public void addCoinButtonClick(View view)
    {
        searchAndAddCoin();
    }

    public void searchAndAddCoin()
    {
        progressDialog.show();
        findViewById(R.id.coinNotExistText).setVisibility(View.INVISIBLE);

        final String coinName = ((EditText)findViewById(R.id.cryptoName)).getText().toString();
        String url = "https://api.coinmarketcap.com/v1/ticker/" + coinName + "/";

        StringRequest coinExistsRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        if (!response.contains("error"))
                        {
                            try
                            {
                                /* Get the coin values */
                                JSONArray json = new JSONArray(response);

                                final String coinShortName = json.getJSONObject(0).getString("id");
                                final String coinLongName = json.getJSONObject(0).getString("name");
                                final String symbol = json.getJSONObject(0).getString("symbol");
                                final float coinValue = (float)json.getJSONObject(0).getDouble("price_usd");
                                final float percentage = ((DiscreteSeekBar)findViewById(R.id.percBar)).getProgress();
                                final float hold;

                                /* Show a coin hold dialog */
                                final AlertDialog.Builder alert = new AlertDialog.Builder(AddCoinActivity.this);
                                final EditText edittext = new EditText(AddCoinActivity.this);

                                alert.setTitle("How many " + coinLongName + " do you have ?");
                                alert.setMessage(" ");
                                alert.setCancelable(false);
                                edittext.setHint("Example: 12.235");
                                alert.setView(edittext);

                                alert.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int whichButton)
                                    {
                                        addCoinToDatabase(new CoinInfos(coinShortName, coinLongName, symbol, alertType, coinValue, percentage, 1, 0, getNewNotificationID(), Float.parseFloat(edittext.getText().toString())));
                                        progressDialog.dismiss();

                                        setResult(RESULT_OK);
                                        finish();
                                    }
                                });

                                alert.setNegativeButton("I DON'T HAVE", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        addCoinToDatabase(new CoinInfos(coinShortName, coinLongName, symbol, alertType, coinValue, percentage, 1, 0f, getNewNotificationID(), 0f));
                                        progressDialog.dismiss();

                                        setResult(RESULT_OK);
                                        finish();
                                    }
                                });

                                alert.show();
                            }
                            catch (JSONException e)
                            {
                                Log.e("Http", "JSON decoding error in addcoin activity: " + e.toString());
                                progressDialog.dismiss();
                            }
                        }
                        else
                        {
                            Log.e("Http", "Coin not found error");
                            textCoinNotExist.setVisibility(View.VISIBLE);
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Http", "Erore accessing CMC coin value: " + error.toString());
                        textCoinNotExist.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                    }
                }
        );

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(coinExistsRequest);
    }

    void addCoinToDatabase(CoinInfos coin)
    {
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        databaseHelper.addData(coin);
    }

    int getNewNotificationID()
    {
        SharedPreferences prefs = getSharedPreferences(getResources().getString(R.string.globalPrefsName), MODE_PRIVATE);
        int newNotificationID = prefs.getInt(getResources().getString(R.string.prefsLastNotificationID), 1000) + 1;

        SharedPreferences.Editor prefsEdit = getSharedPreferences(getResources().getString(R.string.globalPrefsName), MODE_PRIVATE).edit();
        prefsEdit.putInt(getResources().getString(R.string.prefsLastNotificationID), newNotificationID);
        prefsEdit.commit();

        return newNotificationID;
    }
}