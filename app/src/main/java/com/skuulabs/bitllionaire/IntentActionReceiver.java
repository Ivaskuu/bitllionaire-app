package com.skuulabs.bitllionaire;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Adrian on 21/10/2017.
 */

public class IntentActionReceiver extends BroadcastReceiver
{
    public static final String DELETE_OVERLIMIT_NOTIFICATION = "com.skuulabs.bitllionaire.customAction.ON_OVERLIMIT_NOTIFICATION_DELETE";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        if(intent.getAction().equals(DELETE_OVERLIMIT_NOTIFICATION))
        {
            DatabaseHelper databaseHelper = new DatabaseHelper(context);
            databaseHelper.toggleIsShowingNotification(false, intent.getExtras().getString("coinName"));
        }
    }
}