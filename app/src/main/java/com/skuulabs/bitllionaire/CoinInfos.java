package com.skuulabs.bitllionaire;

/**
 * Created by Adrian on 08/10/2017.
 */

public class CoinInfos
{
    public String shortName;
    public String longName;
    public String symbol;
    public int alertType;
    public float value;
    public float percent;
    public int active;
    public float newValue; // Used for the main list
    public int notificationID; // Used for the main list
    public float hold;

    CoinInfos
    (

    )
    {};

    CoinInfos(String shortName, String longName, String symbol, int alertType, float value, float percent, int active, float newValue, int notificationID, float hold)
    {
        this.shortName = shortName;
        this.longName = longName;
        this.symbol = symbol;
        this.alertType = alertType;
        this.value = value;
        this.percent = percent;
        this.active = active;
        this.newValue = newValue;
        this.notificationID = notificationID;
        this.hold = hold;
    }
}
