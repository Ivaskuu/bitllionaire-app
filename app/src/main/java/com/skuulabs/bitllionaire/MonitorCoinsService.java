package com.skuulabs.bitllionaire;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Adrian on 05/10/2017.
 */

public class MonitorCoinsService extends Service
{
    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        SharedPreferences prefs = getSharedPreferences(getResources().getString(R.string.globalPrefsName), MODE_PRIVATE);
        if(prefs.getBoolean(getResources().getString(R.string.prefsRunService), false))
        {
            DatabaseHelper databaseHelper = new DatabaseHelper(this);
            final Cursor coinsData = databaseHelper.getData("");

            ArrayList<CoinInfos> coins = new ArrayList<>();

            while(coinsData.moveToNext())
            {
                coins.add(new CoinInfos(coinsData.getString(0), coinsData.getString(1), coinsData.getString(2), coinsData.getInt(3), coinsData.getFloat(4), coinsData.getFloat(5), coinsData.getInt(6), 0f, coinsData.getInt(7), coinsData.getFloat(9)));
            }

            if(coins.size() > 0)
            {
                for(int i = 0; i < coins.size(); i++)
                {
                    performCoinValueCheck(coins.get(i));
                }

                restartService();
            }
            else
            {

                SharedPreferences.Editor prefsEdit = getSharedPreferences(getResources().getString(R.string.globalPrefsName), MODE_PRIVATE).edit();
                prefsEdit.putBoolean(getResources().getString(R.string.prefsRunService), false);
                prefsEdit.commit();
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void performCoinValueCheck(final CoinInfos coinInfos)
    {
        String url = "https://api.coinmarketcap.com/v1/ticker/" + coinInfos.shortName + "/";
        StringRequest btcPriceRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        // Checks the new coin value
                        try
                        {
                            JSONArray json = new JSONArray(response);
                            float newCoinValue = (float)json.getJSONObject(0).getDouble("price_usd");

                            if(coinInfos.active == 1)
                            {
                                if(-getPercentDiff(coinInfos.value, newCoinValue) > coinInfos.percent) /* -(-6.37) > 5 => 6.37 > 5 */
                                {
                                    showOverLimitNotification(coinInfos, getPercentDiff(coinInfos.value, newCoinValue));
                                }
                                else if(-getPercentDiff(coinInfos.value, newCoinValue) > (coinInfos.percent - 0.5f))
                                {
                                    showCloseLimitNotification(coinInfos, getPercentDiff(coinInfos.value, newCoinValue));
                                }
                                else
                                {
                                    /* Delete the notification if the coin perc is higher than the limit and the limit - 0.5% */
                                    NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                                    notificationManager.cancel(coinInfos.notificationID);
                                }
                            }
                        }
                        catch (JSONException e)
                        {
                            Log.e("Http", "Non riesco a decodificare il JSON", e);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Http", "Erore accessing CMC coin value");
                    }
                }
        );

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(btcPriceRequest);
    }

    float getPercentDiff(float oldValue, float newValue)
    {
        return ((newValue - oldValue) / oldValue) * 100;
    }

    private void showCloseLimitNotification(CoinInfos coin, float perc)
    {
        String message = String.format(Resources.getSystem().getConfiguration().locale, "%.2f", perc) + "% : " + coin.longName + "'s price is approaching the limit";

        Intent openMainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent mainActivityPendingIntent = PendingIntent.getActivity(this, 0, openMainActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bundle b = new Bundle();
        b.putString("shortName", coin.shortName);
        b.putString("longName", coin.longName);
        b.putString("symbol", coin.symbol);
        b.putInt("alertType", coin.alertType);
        b.putFloat("value", coin.value);
        b.putFloat("percent", coin.percent);
        b.putInt("active", coin.active);
        b.putInt("notificationID", coin.notificationID);

        Intent coinIntent = new Intent(MonitorCoinsService.this, CoinActivity.class);
        coinIntent.putExtras(b);
        coinIntent.setAction(Long.toString(System.currentTimeMillis()));
        PendingIntent coinPendingIntent = PendingIntent.getActivity(this, 0, coinIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        /* Show the notification */
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_attach_money_white_24dp)
                .setContentText(message)
                .setColor(Color.argb(255, 255, 0, 0))
                .setContentIntent(coinPendingIntent)
                .setOngoing(true)
                .setWhen(System.currentTimeMillis())
                .addAction(new NotificationCompat.Action(R.drawable.ic_attach_money_white_24dp, "STOP MONITORING", mainActivityPendingIntent))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(coin.notificationID, notificationBuilder.build());

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        databaseHelper.toggleIsShowingNotification(false, coin.shortName);
    }

    private void showOverLimitNotification(final CoinInfos coin, float perc)
    {
        /* Init the (custom) remote view */
        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification);

        remoteViews.setTextViewText(R.id.notificationTitle, coin.longName.length() < 15
                ? coin.longName + "'s price is over limit" // Full name
                : coin.shortName.substring(0, 1).toUpperCase() + coin.shortName.substring(1) + "'s price is over limit"); // Short name Capitalized

        remoteViews.setTextViewText(R.id.notificationValue, String.format(Resources.getSystem().getConfiguration().locale, "%.2f", perc) + "%");

        Bundle b = new Bundle();
        b.putString("shortName", coin.shortName);
        b.putString("longName", coin.longName);
        b.putString("symbol", coin.symbol);
        b.putInt("alertType", coin.alertType);
        b.putFloat("value", coin.value);
        b.putFloat("percent", coin.percent);
        b.putInt("active", coin.active);
        b.putInt("notificationID", coin.notificationID);

        /* Open the coin activity on notification click */
        Intent coinIntent = new Intent(MonitorCoinsService.this, CoinActivity.class);
        coinIntent.putExtras(b);
        coinIntent.setAction(Long.toString(System.currentTimeMillis()));
        PendingIntent coinPendingIntent = PendingIntent.getActivity(this, 0, coinIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent deleteNotificationIntent = new Intent(MonitorCoinsService.this, IntentActionReceiver.class);
        deleteNotificationIntent.putExtra("coinName", coin.shortName);
        deleteNotificationIntent.setAction(IntentActionReceiver.DELETE_OVERLIMIT_NOTIFICATION);
        PendingIntent deleteNotificationPendingIntent = PendingIntent.getBroadcast(this, 0, deleteNotificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        /* Show the notification */
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_attach_money_white_24dp)
                .setContentTitle("Coin alert")
                .setContentIntent(coinPendingIntent)
                .setWhen(System.currentTimeMillis())
                .setCustomContentView(remoteViews)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setDeleteIntent(deleteNotificationPendingIntent)
                .setAutoCancel(true)
                .setGroup("Coin alert")
                .setGroupSummary(true);

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        if(!databaseHelper.getIsShowingNotification(coin.shortName)) notificationBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);

        notificationManager.notify(coin.notificationID, notificationBuilder.build());
        databaseHelper.toggleIsShowingNotification(true, coin.shortName);
    }

    private void restartService()
    {
        Intent serviceIntent = new Intent(this, MonitorCoinsService.class);
        PendingIntent servicePendingIntent = PendingIntent.getService(this, MainActivity.coinPriceUpdateIntentID, serviceIntent, 0);

        AlarmManager alarmManager = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + MainActivity.COINS_CHECK_INTERVAL, servicePendingIntent);

        Log.d("Serviice", "Restart service");
    }
}