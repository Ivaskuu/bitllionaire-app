package com.skuulabs.bitllionaire;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.buglife.sdk.Buglife;
import com.buglife.sdk.InvocationMethod;
import com.buglife.sdk.PickerInputField;
import com.buglife.sdk.TextInputField;
import com.codemybrainsout.ratingdialog.RatingDialog;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.github.fabtransitionactivity.SheetLayout;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import okhttp3.OkHttpClient;

import static com.skuulabs.bitllionaire.R.id.fab;

public class MainActivity extends AppCompatActivity implements SheetLayout.OnFabAnimationEndListener
{
    public static int coinPriceUpdateIntentID = 25;

    public static final int COINS_CHECK_INTERVAL = 1 * 60 * 1000;

    public static final int NEW_COIN_REQUEST_CODE = 1001;
    public static final int COIN_ACTIVITY_REQUEST_CODE = 1002;

    private int coinsNum;
    private ArrayList<CoinInfos> coinsInfos = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private CoinsAdapter adapter;
    private SnapHelper snapHelper;

    private ProgressDialog progressDialog;
    private SheetLayout mSheetLayout;
    private FloatingActionButton addCoinFab;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        /* Load the onboarding page */
        if(!getSharedPreferences(getResources().getString(R.string.globalPrefsName), MODE_PRIVATE).getBoolean(getResources().getString(R.string.prefsOnboard), false))
        {
            Intent intent = new Intent(MainActivity.this, OnboardingActivity.class);
            startActivity(intent);
            finish();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Stetho */
        Stetho.initializeWithDefaults(this);
        new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build();

        /* Init the progress dialog */
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Getting the coin values");
        progressDialog.setMessage("Give us a second ...");
        progressDialog.setCancelable(false);

        /* FAB reveal animation */
        mSheetLayout = (SheetLayout)findViewById(R.id.bottom_sheet);
        addCoinFab = (FloatingActionButton)findViewById(fab);

        mSheetLayout.setFab(addCoinFab);
        mSheetLayout.setFabAnimationEndListener(this);

        snackbar = Snackbar.make(this.findViewById(R.id.topRelativeLayout), R.string.no_internet, Snackbar.LENGTH_INDEFINITE);

        /* Delete all notifications */
        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        /* Init the toolbar */
        setSupportActionBar((Toolbar)this.findViewById(R.id.mainToolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        initBugReporting();
        showRatingDialog();
        initRecyclerView();

        loadCoins();
    }

    private void initBugReporting()
    {
        Buglife.initWithApiKey(getApplication(), "HrakbdgOw2u3clY9bnT1Iwtt");
        Buglife.setInvocationMethod(InvocationMethod.SCREENSHOT);

        TextInputField whatField = new TextInputField("What happened ?");

        PickerInputField urgencyField = new PickerInputField("Is it critical ?");
        urgencyField.addOption("Yes");
        urgencyField.addOption("No");

        TextInputField stepsField = new TextInputField("The steps to reproduce this");
        stepsField.setMultiline(true);

        TextInputField emailField = new TextInputField("Your email for further contact (optional)");

        Buglife.setInputFields(whatField, urgencyField, stepsField, emailField);
    }

    private void showRatingDialog()
    {
        final RatingDialog ratingDialog = new RatingDialog.Builder(this)
                .session(3)
                .threshold(3)
                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener()
                {
                    @Override
                    public void onFormSubmitted(String feedback) {

                    }
                }).build();
        ratingDialog.show();
    }

    private void initRecyclerView()
    {
        recyclerView = (RecyclerView)this.findViewById(R.id.coinsRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);
    }

    private void startService()
    {
        /* Create a alarmmanager to start the monitoring service intent */
        Intent monitoringServiceIntent = new Intent(this, MonitorCoinsService.class);
        PendingIntent monitoringServicePendingIntent = PendingIntent.getService(this, MainActivity.coinPriceUpdateIntentID, monitoringServiceIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + COINS_CHECK_INTERVAL, monitoringServicePendingIntent);

        Toast.makeText(getApplicationContext(), "Monitoring started !", Toast.LENGTH_SHORT).show();
    }

    private void loadCoins()
    {
        snackbar.dismiss();

        coinsInfos.clear();
        recyclerView.setVisibility(View.VISIBLE);
        this.findViewById(R.id.moneyToolbar).setVisibility(View.VISIBLE);

        /* Load coins from the database */
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        final Cursor coinsData = databaseHelper.getData("");

        while (coinsData.moveToNext())
        {
            coinsInfos.add(new CoinInfos(coinsData.getString(0), coinsData.getString(1), coinsData.getString(2), coinsData.getInt(3), coinsData.getFloat(4), coinsData.getFloat(5), coinsData.getInt(6), 0f, coinsData.getInt(7), coinsData.getFloat(9)));
        }

        /* Start the service if at least a coin is active and service is disabled */
        boolean atLeastOneCoinActive = false;
        if(coinsInfos.size() > 0)
        {
            for(int i = 0; i < coinsInfos.size(); i++)
            {
                if(coinsInfos.get(i).active == 1) atLeastOneCoinActive = true; break;
            }
        }

        if(coinsInfos.size() > 0 && atLeastOneCoinActive)
        {
            SharedPreferences prefsView = getSharedPreferences(getResources().getString(R.string.globalPrefsName), MODE_PRIVATE);
            if(!prefsView.getBoolean(getResources().getString(R.string.prefsRunService), false))
            {
                SharedPreferences.Editor prefsEdit = getSharedPreferences(getResources().getString(R.string.globalPrefsName), MODE_PRIVATE).edit();
                prefsEdit.putBoolean(getResources().getString(R.string.prefsRunService), true);
                prefsEdit.commit();

                startService();
            }
        }
        else // No coins or all coins deactivated
        {
            SharedPreferences prefsView = getSharedPreferences(getResources().getString(R.string.globalPrefsName), MODE_PRIVATE);
            if(prefsView.getBoolean(getResources().getString(R.string.prefsRunService), false))
            {
                SharedPreferences.Editor prefsEdit = getSharedPreferences(getResources().getString(R.string.globalPrefsName), MODE_PRIVATE).edit();
                prefsEdit.putBoolean(getResources().getString(R.string.prefsRunService), false);
                prefsEdit.commit();
            }
        }

        /* Check for an internet connection : if not connected to the internet add the coins from db */
        if(!isConnectedToInternet())
        {
            addCoinFab.setVisibility(View.GONE);
            snackbar.show();

            addCoinsToListView();
        }
        else /* Else if connected to the internet download the coins values */
        {
            addCoinFab.setVisibility(View.VISIBLE);
            if(coinsInfos.size() > 0) /* If there is at least one coin */
            {
                progressDialog.show();

                coinsNum = coinsInfos.size();
                for(int i = 0; i < coinsInfos.size(); i++) /* Download the coin infos for each coin */
                {
                    downloadCoinValue(coinsInfos.get(i));
                }
            }
            else /* Else show the placeholder if there are no coins to monitor */
            {
                recyclerView.setVisibility(View.GONE);
                this.findViewById(R.id.moneyToolbar).setVisibility(View.GONE);
            }
        }
    }

    private void downloadCoinValue(final CoinInfos coin)
    {
        String url = "https://api.coinmarketcap.com/v1/ticker/" + coin.shortName + "/";
        StringRequest btcPriceRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        // Checks the new coin value
                        try
                        {
                            JSONArray json = new JSONArray(response);
                            coin.newValue = (float)json.getJSONObject(0).getDouble("price_usd");

                            if(coinsNum == 1) /* The last coin to have downloaded the values */
                            {
                                progressDialog.dismiss();
                                addCoinsToListView();
                            }
                            else
                            {
                                coinsNum--;
                            }
                        }
                        catch (JSONException e)
                        {
                            Log.e("Http", "Non riesco a decodificare il JSON", e);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Http", "Erore accessing CMC coin value");
                    }
                }
        );

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(btcPriceRequest);
    }

    public void onFabClick(View view)
    {
        mSheetLayout.expandFab();
    }

    @Override
    public void onFabAnimationEnd()
    {
        openAddCoinActivity();
    }

    public void openAddCoinActivity()
    {
        Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle();

        Intent openAddCoinActivityIntent = new Intent(this, AddCoinActivity.class);
        startActivityForResult(openAddCoinActivityIntent, NEW_COIN_REQUEST_CODE, bundle);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == NEW_COIN_REQUEST_CODE)
        {
            mSheetLayout.contractFab();

            if(resultCode == Activity.RESULT_OK)
            {
                loadCoins();
            }
        }
        else if(requestCode == COIN_ACTIVITY_REQUEST_CODE)
        {
            if(resultCode == RESULT_OK) loadCoins();
        }
    }

    private void addCoinsToListView()
    {
        adapter = new CoinsAdapter(coinsInfos);
        recyclerView.setAdapter(adapter);

        float totalMoney = 0f;
        for (int i = 0; i < coinsInfos.size(); i++)
        {
            Log.d("forr", "Hold : " + coinsInfos.get(i).hold);
            totalMoney += coinsInfos.get(i).newValue * coinsInfos.get(i).hold;
        }

        ((TextView)this.findViewById(R.id.totalMoneyText)).setText("$ " + String.format(Resources.getSystem().getConfiguration().locale, "%.2f", totalMoney));
    }

    public boolean isConnectedToInternet()
    {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == R.id.menu_refresh)
        {
            loadCoins();
        }
        else if(item.getItemId() == R.id.menu_sort)
        {
            Toast.makeText(this, "Menu sort, to be done ...", Toast.LENGTH_SHORT).show();
        }
        else if(item.getItemId() == R.id.menu_about)
        {
            Toast.makeText(this, "Menu about, to be done ...", Toast.LENGTH_SHORT).show();
        }
        else if(item.getItemId() == R.id.menu_settings)
        {
            Toast.makeText(this, "Menu settings, to be done ...", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }
}