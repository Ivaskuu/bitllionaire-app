package com.skuulabs.bitllionaire;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import az.plainpie.PieView;

/**
 * Created by Adrian on 21/10/2017.
 */

public class CoinsAdapter extends RecyclerView.Adapter<CoinsAdapter.ViewHolder>
{
    private ArrayList<CoinInfos> coins;
    private Context context;

    public CoinsAdapter(ArrayList<CoinInfos> coins)
    {
        this.coins = coins;
    }

    @Override
    public CoinsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.coin_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CoinsAdapter.ViewHolder holder, int position)
    {
        /* Set the symbol and the name */
        final CoinInfos thisCoin = coins.get(position);
        holder.symbolText.setText(thisCoin.symbol);
        holder.longNameText.setText(thisCoin.longName);

        /* Set the pie chart colors */
        holder.pie.setInnerBackgroundColor(Color.WHITE);
        holder.pie.setMainBackgroundColor(Color.parseColor("#4b92fe"));

        /* Calculate the perc */
        float perc = ((thisCoin.newValue - thisCoin.value) / thisCoin.value) * 100;

        /* Prevents the all blue pie */
        if(perc == 0)
        {
            perc += 0.001f;
        }

        holder.pie.setPercentage(perc);

        if(perc >= 0) /* Green */
        {
            holder.percText.setText("+" + String.format(Resources.getSystem().getConfiguration().locale, "%.2f", perc) + "%");
            holder.percText.setTextColor(Color.parseColor("#42ee73"));
            holder.pie.setPercentageBackgroundColor(Color.parseColor("#42ee73"));
        }
        else /* Red */
        {
            holder.percText.setText(String.format(Resources.getSystem().getConfiguration().locale, "%.2f", perc) + "%");
            holder.percText.setTextColor(Color.parseColor("#fe4343"));
            holder.pie.setPercentageBackgroundColor(Color.parseColor("#fe4343"));
        }

        if(thisCoin.active == 0)
        {
            holder.symbolText.setText(thisCoin.symbol + " (off)");
            holder.symbolText.setTextColor(Color.parseColor("#808080"));
            holder.percText.setTextColor(Color.parseColor("#cccccc"));
            holder.pie.setMainBackgroundColor(Color.parseColor("#cccccc"));
        }

        holder.card.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Bundle b = new Bundle();
                b.putString("shortName", thisCoin.shortName);
                b.putString("longName", thisCoin.longName);
                b.putInt("alertType", thisCoin.alertType);
                b.putFloat("value", thisCoin.value);
                b.putFloat("percent", thisCoin.percent);
                b.putInt("active", thisCoin.active);
                b.putInt("notificationID", thisCoin.notificationID);

                Intent intent = new Intent(context, CoinActivity.class);
                intent.putExtras(b);
                ((MainActivity)context).startActivityForResult(intent, MainActivity.COIN_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return coins.size();
    }

    /* The "UI" elements of the layout */
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView symbolText;
        public TextView longNameText;
        public TextView percText;
        public PieView pie;
        public CardView card;

        public ViewHolder(View itemView)
        {
            super(itemView);
            symbolText = (TextView) itemView.findViewById(R.id.listItemCoinShortName);
            longNameText = (TextView) itemView.findViewById(R.id.listItemCoinFullName);
            percText = (TextView) itemView.findViewById(R.id.percText);
            pie = (PieView) itemView.findViewById(R.id.pieChart);
            card = (CardView) itemView.findViewById(R.id.coinCard);

            context = itemView.getContext();
        }
    }
}
